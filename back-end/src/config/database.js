module.exports = {
  development: {
    username: 'docker',
    password: 'todolist',
    database: 'todo',
    host: 'db',
    dialect: 'mysql',
    logging: false,
  },
  test: {
    username: 'docker',
    password: 'todolist',
    database: 'todo',
    host: 'db',
    dialect: 'mysql',
    logging: false,
  },
  production: {
    username: 'docker',
    password: 'todolist',
    database: 'todo',
    host: 'db',
    dialect: 'mysql',
    logging: false,
  },
};
