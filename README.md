# Utilizando variaveis de ambientes:

1 - Crie um arquivo .env na pasta raiz do projeto

2 - Crie as variaveis as quais você deseja referenciar algo, por exemplo
variaveis voltados para o back-end: 
Estrutura - NOME_DA_VARIAVEL = VALOR
MYSQL_PASSWORD = SenhaDoBanco, MYSQL_USER = UsuarioDoBanco

3 - Para referenciar a estas variaveis basta seguir essa estrutura:
Estrutura - ${NOME_DAVARIAVEL}
Ex  -  ${MYSQL_USER}


# Instanciando conteiners no Docker

Comando para instaciar todos os containers presentes no projeto que são o BD, o backend
e front-end.

docker-compose up

# Rodando back-end
Para entrar no conteiner do back-end:
docker exec -it todo-list-api bash

Para rodar o backend:
npm start;

# Rodando front-end

Para entrar no conteiner front-end:
docker exec -it todo-list-ui bash

Para rodar o front-end:
npm start;

# Rodando tests:
Primeiro entramos no conteiner do back-end:
docker exec -it todo-list-api bash

Para rodar os testes:
npm test




